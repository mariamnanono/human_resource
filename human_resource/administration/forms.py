from django.forms import forms

from administration.models import UserRole, WEEKDAYS


class UserRolesForm(forms.Model):
    class Meta:
        role_name = forms.CharField(max_length=50, unique=True)
        description = forms.TextField(null=True, blank=True)
        active = forms.BooleanField(default=True)


class Employee(forms.Model):
    class Meta:
        employee_name = forms.CharField(max_length=100, unique=True)
        image = forms.ImageField(upload_to='media/employee_images')
        user_role = forms.ForeignKey(UserRole)


class User(forms.Model):
    class Meta:
        username = forms.CharField(max_length=100, unique=True)
        user_role = forms.ForeignKey(UserRole)
        employee_name = forms.ForeignKey(Employee)
        status = forms.BooleanField(default=True)
        password = forms.CharField(max_length=100, unique=True)
        confirm_password = forms.CharField(max_length=100, unique=True)


class JobTitle(forms.Model):
    class Meta:
        title = forms.CharField(max_length=100, unique=True)
        description = forms.CharField(max_length=100, unique=True)
        specification = forms.FileField(upload_to='media/jobs_specifications')


class JobCategory(forms.Model):
    class Meta:
        category_name = forms.CharField(max_length=100, unique=True)

class WorkShift(forms.Model):
    class Meta:
        shift_name = forms.CharField(max_length=100, unique=True)
        weekday = forms.IntegerField(choices=WEEKDAYS)
        from_hour = forms.TimeField()
        to_hour = forms.TimeField()