from django.contrib import admin

from administration.models import UserRole, Employee, User, JobTitle, JobCategory, WorkShift

admin.site.register(UserRole)
admin.site.register(Employee)
admin.site.register(User)
admin.site.register(JobTitle)
admin.site.register(JobCategory)
admin.site.register(WorkShift)