from django.db import models


class UserRole(models.Model):
    role_name = models.CharField(max_length=50, unique=True)
    description = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.role_name


class Employee(models.Model):
    employee_name = models.CharField(max_length=100, unique=True)
    image = models.ImageField(upload_to='media/employee_images')
    user_role = models.ForeignKey(UserRole)

    def __str__(self):
        return self.employee_name

    def get_image_url(self):
        img = self.employeeimage_set.first()
        if img:
            return img.image.url
        return img


class User(models.Model):
    username = models.CharField(max_length=100, unique=True)
    user_role = models.ForeignKey(UserRole)
    employee_name = models.ForeignKey(Employee)
    status = models.BooleanField(default=True)
    password = models.CharField(max_length=100, unique=True)
    confirm_password = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.username


class JobTitle(models.Model):
    title = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=100, unique=True)
    specification = models.FileField(upload_to='media/jobs_specifications')

    def __str__(self):
        return self.title


class JobCategory(models.Model):
    category_name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.category_name





WEEKDAYS = [
  (1, ("Monday")),
  (2, ("Tuesday")),
  (3, ("Wednesday")),
  (4, ("Thursday")),
  (5, ("Friday")),
  (6, ("Saturday")),
  (7, ("Sunday")),
]

class WorkShift(models.Model):
    shift_name = models.CharField(max_length=100, unique=True)
    weekday = models.IntegerField(choices=WEEKDAYS)
    from_hour = models.TimeField()
    to_hour = models.TimeField()

    class Meta:
        ordering = ('weekday', 'from_hour')
        unique_together = ('weekday', 'from_hour', 'to_hour')

    def __str__(self):
        return u'%s: %s - %s' % (self.get_weekday_display(),
                                 self.from_hour, self.to_hour)

    def __str__(self):
        return self.shift_name