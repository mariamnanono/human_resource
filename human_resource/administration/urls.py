# import user

from django.conf.urls import url
from . import views

urlpatterns = [
    # user roles urls

    url(r'^$', views.UserRoleListView.as_view(), name='user_role_landing'),
    url(r"create$", views.CreateUserRoleView.as_view(), name='create_user_role'),

    #employee urls
    url(r'^$', views.EmployeeListView.as_view(), name='employee_landing'),
    url(r"create$", views.CreateEmployeeView.as_view(), name='create_employee'),

    # system users urls
    url(r'^$', views.UserListView.as_view(), name='user_landing'),
    url(r"create$", views.CreateUserView.as_view(), name='create_users'),

    # job titles urls
    url(r'^$', views.JobTitleListView.as_view(), name='job_title_landing'),
    url(r"create$", views.CreateUserView.as_view(), name='create_job_title'),

    # job category urls
    url(r'^$', views.JobCategoryListView.as_view(), name='job_category_landing'),
    url(r"create$", views.CreateJobCategoryView.as_view(), name='create_job_category'),

    # work shifts urls
    url(r'^$', views.WorkShiftListView.as_view(), name='work_shift_landing'),
    url(r"create$", views.CreateWorkShiftView.as_view(), name='create_work_shift'),

]
