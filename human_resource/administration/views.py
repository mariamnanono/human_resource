# Create your views here.

from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView

from administration.models import UserRole, User, JobTitle, JobCategory, WorkShift


class CreateUserRoleView(CreateView):
    model = UserRole
    fields = ['role_name', 'description', 'active']
    success_url = reverse_lazy("administration:user_role_landing")
    template_name = 'administration/create_user_role.html'


class UserRoleListView(ListView):
    model = UserRole
    template_name = 'administration/user_role_landing.html'


class CreateEmployeeView(CreateView):
    model = UserRole
    fields = ['employee_name', 'image', 'user_role']
    success_url = reverse_lazy("administration:employee_landing")
    template_name = 'administration/create_employee.html'


class EmployeeListView(ListView):
    model = UserRole
    template_name = 'administration/employee_landing.html'


class CreateUserView(CreateView):
    model = User
    fields = ['username', 'user_role', 'employee_name', ' status', 'password', ' confirm_password']
    success_url = reverse_lazy("administration:user_landing")
    template_name = 'admininstration/create_users.html'


class UserListView(ListView):
    model = User
    template_name = 'administration/user_landing.html'


class CreateJobTitleView(CreateView):
    model = JobTitle
    fields = ['title', 'description', 'specification', ' status', 'password', ' confirm_password']
    success_url = reverse_lazy("administration:job_title_landing")
    template_name = 'admininstration/create_job_title.html'


class JobTitleListView(ListView):
    model = JobTitle
    template_name = 'administration/job_title_landing.html'



class CreateJobCategoryView(CreateView):
    model = JobCategory
    success_url = reverse_lazy("administration:job_category_landing")
    template_name = 'administration/create_job_category.html'

class JobCategoryListView(ListView):
    model = JobCategory
    template_name = 'administration/job_category_landing.html'

class CreateWorkShiftView(CreateView):
    model = WorkShift
    fields = ['shift_name','weekday','from_hour',' to_hour']
    success_url = reverse_lazy('administration:work_shift_landing')
    template_name = 'administration/create_work_shift.html'

class WorkShiftListView(ListView):
    model = WorkShift
    template_name = 'administration/work_shift_landing.html'




